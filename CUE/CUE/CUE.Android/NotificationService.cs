﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using CUE.Droid;
using CUE.interfaces;
using Xamarin.Forms;
[assembly: Dependency(typeof(NotificationService))]
namespace CUE.Droid
{
    public class NotificationService : INotification
    {
        public void Notify(
                string message,
                int duration,
                string actionText,
                Action<object> action)
        {
            var view = ((Activity)Forms.Context).FindViewById(Android.Resource.Id.Content);
            var snack = Snackbar.Make(view, message, duration);
            if (actionText != null && action != null)
                snack.SetAction(actionText, action);
            snack.Show();
        }
    }

}