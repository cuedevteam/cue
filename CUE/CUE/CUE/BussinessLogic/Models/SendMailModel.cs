﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.BussinessLogic.Models
{
   public class SendMailModel
    {
        public bool Status { get; set; }
        public string Status_message { get; set; }
        public string Otp { get; set; }
    }
}
