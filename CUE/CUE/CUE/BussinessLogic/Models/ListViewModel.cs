﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.BussinessLogic.Models
{
    public class ListViewModel
    {
        //From_date
        public string Data { get; set; }
        public string Time { get; set; }
        public string Header { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Id { get; set; }
        public string User_id { get; set; }
        public string Cat_id { get; set; }
        public string Exp_date { get; set; }
        public string Remainder { get; set; }
        public string Flag { get; set; }
        public string Prize { get; set; }
        public string Url { get; set; }
    }
}
