﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.BussinessLogic.Models
{
   public class UserUsing_user_idModel
    {
        public string User_id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Dbo { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string Area { get; set; }
        public string Zip_code { get; set; }
        public string Family_members { get; set; }
        public string Brothers { get; set; }
        public string Prof_url { get; set; }
    }
}
