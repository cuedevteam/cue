﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.BussinessLogic
{
   public class AllCompletEventsModel
    {
        public string Id { get; set; }
        public string User_id { get; set; }
        public string Cat_id { get; set; }
        public string From_date { get; set; }
        public string Exp_date { get; set; }
        public string Remainder { get; set; }
        public string Flag { get; set; }
        public string Prize { get; set; }
        public string Url { get; set; }
    }
}
