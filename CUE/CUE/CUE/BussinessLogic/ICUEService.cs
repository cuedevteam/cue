﻿using CUE.BussinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CUE.BussinessLogic
{
    public interface ICUEService
    {
        //bizz-manager.com/cue/index.php/Userdata/Login/email_here/password_here
        Task<List<LoginModels>> Login(string email, string passwrd);
        //bizz-manager.com/cue/index.php/Userdata/register/username_here/password_here/phone_here
        Task<RegisterModel> RegisterAsync(string username_here, string password_here, string phone_here);
        //bizz-manager.com/cue/index.php/Userdata/userid_here
        Task<List<UserUsing_user_idModel>> Userdata(string username_here);
        //bizz-manager.com/cue/index.php/Eventsdata/AllCompletEvents/userid_here
        Task<List<AllCompletEventsModel>> AllCompletEvents(string username_here);
        //bizz-manager.com/cue/index.php/Eventsdata/CompletEvents_using_id/userid_here/eventId_here
        Task CompletEvents(string username_here, string eventId_here);
        //bizz-manager.com/cue/index.php/Pdfdata/AllPdfData/
        Task<List<AllPdfDataModel>> AllPdfData();
        //bizz-manager.com/cue/index.php/Pdfdata/PdfUsing_user_id/User_Id_here
        Task PdfByUserId(string username_here);
        //bizz-manager.com/cue/index.php/Pdfdata/PdfUsing_Id/Doc_Id_here
        Task DocByDoc_Id(string Doc_Id_here);
        //bizz-manager.com/cue/index.php/Userdata/SendMail/happycoding510@gmail.com
        Task<RegisterModel> SendMail(string emailId);
        //bizz-manager.com/cue/index.php/Eventsdata/AllCompletEvents/userid_here
        Task<List<AllCompletEventsModel>> CompletedEvents(string username_here,string event_id);


    }
}
