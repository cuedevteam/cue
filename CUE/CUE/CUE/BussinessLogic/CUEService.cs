﻿using CUE.BussinessLogic.Models;
using CUE.utills;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CUE.BussinessLogic
{
    public class CUEService : ICUEService
    {
        public HttpClient client;

        public CUEService()
        {
            // var authData = string.Format("{0}:{1}", Constants.Username, Constants.Password);
            // var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            client = new HttpClient
            {
                MaxResponseContentBufferSize = 256000
            };
            //  client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public async Task<List<AllCompletEventsModel>> AllCompletEvents(string user_id)
        {

            var uri = new Uri(string.Format(Constants.RestUrl + Constants.AllCompletEvents, user_id));
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<AllCompletEventsModel> allCompletEvents = JsonConvert.DeserializeObject<List<AllCompletEventsModel>>(content);
                    return allCompletEvents;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return null;

        }

        public async Task<List<AllCompletEventsModel>> CompletedEvents(string user_id, string Event_id)
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.CompletEvents, user_id, Event_id));
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<AllCompletEventsModel> CompletedEvents = JsonConvert.DeserializeObject<List<AllCompletEventsModel>>(content);
                    return CompletedEvents;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return null;


        }

        public async Task<List<AllPdfDataModel>> AllPdfData()
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.AllPdfData));
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<AllPdfDataModel> allpdfData = JsonConvert.DeserializeObject<List<AllPdfDataModel>>(content);
                    return allpdfData;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return null;
        }

        public Task CompletEvents(string username_here, string eventId_here)
        {
            throw new NotImplementedException();
        }

        public Task DocByDoc_Id(string Doc_Id_here)
        {
            throw new NotImplementedException();
        }

        public async Task<List<LoginModels>> Login(string email, string passwrd)
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.Userdata_login, email, passwrd));
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    List<LoginModels> loginresp = JsonConvert.DeserializeObject<List<LoginModels>>(content);
                    return loginresp;
                }

            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return null;
        }

        public Task PdfByUserId(string username_here)
        {
            throw new NotImplementedException();
        }

        public async Task<RegisterModel> RegisterAsync(string username_here, string password_here, string phone_here)
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.Userdata_register, username_here, password_here, phone_here));
            RegisterModel registerModel = null;
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    registerModel = JsonConvert.DeserializeObject<RegisterModel>(content);
                }
                return registerModel;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return registerModel;
        }

        public async Task<RegisterModel> SendMail(string emailId)
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.SendMail, emailId));
            RegisterModel registerModel = null;
            try
            {
                var response = await client.GetAsync(uri);
                //var response = await client.GetAsync("http://bizz-manager.com/cue/index.php/Userdata/SendMail/vishnutejar@gmail.com");
                //http://bizz-manager.com/cue/index.php/Userdata/SendMail/vishnutejar@gmail.com
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    registerModel = JsonConvert.DeserializeObject<RegisterModel>(content);
                }
                return registerModel;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return registerModel;
        }

        public async Task<List<UserUsing_user_idModel>> Userdata(string username_here)
        {
            var uri = new Uri(string.Format(Constants.RestUrl + Constants.Userdata, username_here));
            List<UserUsing_user_idModel> userData = null;
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    userData = JsonConvert.DeserializeObject<List<UserUsing_user_idModel>>(content);
                }
                return userData;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);

            }
            return userData;
        }
    }
}
