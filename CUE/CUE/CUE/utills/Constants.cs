﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.utills
{
   public static class Constants
    {
        // URL of Base Service
        public static string RestUrl = "https://bizz-manager.com/cue/index.php/";

       // Userdata/Login/email_here/password_here
        public static string Userdata_login = "Userdata/Login/{0}/{1}";
        //Userdata/register/username_here/password_here/phone_here
        public static string Userdata_register = "Userdata/register/{0}/{1}/{2}";      
        //bizz-manager.com/cue/index.php/Userdata/userid_here
        public static string Userdata = "Userdata/UserUsing_user_id/{0}";
        //Eventsdata/AllCompletEvents/userid_here
        public static string AllCompletEvents = "Eventsdata/AllCompletEvents/{0}";
        //Eventsdata/CompletEvents_using_id/userid_here/eventId_here
        public static string CompletEvents = "Eventsdata/CompletEvents_using_id/{0}/{1}";
        //Pdfdata/AllPdfData/
        public static string AllPdfData = "Pdfdata/AllPdfData";
        //Pdfdata/PdfUsing_user_id/User_Id_here
        public static string PdfUsing_user_id = "Pdfdata/PdfUsing_user_id/{0}";
        //Pdfdata/PdfUsing_Id/Doc_Id_here
        public static string PdfUsing_Id = "Pdfdata/PdfUsing_Id/{0}";
        
        //Userdata/SendMail/{userid_here}or {user_id}
        //Userdata/SendMail/happycoding510@gmail.com
        public static string SendMail = "Userdata/SendMail/{0}";




    }
}
