﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
        ObservableCollection<string> Data;
        public SettingsPage ()
		{
			InitializeComponent ();
            Data = new ObservableCollection<string> { "Sound","Vibration" , "Language" ,"Reminder","About" };
            settingsitems.ItemsSource = Data;
            settingsitems.ItemSelected += SelectedSettings;
        }

        private void SelectedSettings(object sender, SelectedItemChangedEventArgs e)
        {
            settingsitems.SelectedItem = null;
        }
    }
}