﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CUE.pages;
using CUE.BussinessLogic;
using Plugin.Connectivity;
using CUE.BussinessLogic.Models;
namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new SignUpPage());
            // Navigation.PushAsync(new SignUpPage(), true);
        }
        public async Task GoToHomeAsync(object sender, EventArgs e)
        {
            //check entry data is empty show msg else go to menupage
            string Email = email.Text, Password = password.Text;
            if (string.IsNullOrEmpty(Email))
            {

                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Email Id", 5000, null, null);
            }
            else if (string.IsNullOrEmpty(Password))
            {

                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Password", 5000, null, null);
            }
            else
            {
                //call login service 

                if (CrossConnectivity.Current.IsConnected)
                {
                    indicater.IsVisible = true;
                    CUEService uEService = new CUEService();
                    List<LoginModels> loginresp = await uEService.Login(Email, Password);
                    if (loginresp != null)
                    {
                        if (!string.IsNullOrEmpty(loginresp[0].User_id))
                        {
                            //For Saving Value
                            Application.Current.Properties["user_id"] = loginresp[0].User_id;
                            await Application.Current.SavePropertiesAsync();

                            //check is responce status is success go to manenu or ask to user login .
                            await Navigation.PushModalAsync(new MenuPage());

                            DependencyService.Get<CUE.interfaces.INotification>().Notify("Login Successfully, Welcome To CUE APP", 5000, null, null);

                            indicater.IsVisible = false;

                        }
                        else
                        {
                            DependencyService.Get<CUE.interfaces.INotification>().Notify("Check Your User and password", 5000, null, null);

                            indicater.IsVisible = false;
                        }
                    }
                }

            }

            //Navigation.PushAsync(new MenuPage(), true);

        }

        private void Undo()
        {

        }

        void GoToForgotPass(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new ForgotPasswordPage());
            //Navigation.PushAsync(new ForgotPasswordPage());
        }
    }
}