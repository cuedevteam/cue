﻿using CUE.BussinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NotificationListPage : ContentPage
	{
		public NotificationListPage ()
		{
			InitializeComponent ();
            ObservableCollection<string> observbleCollection = new ObservableCollection<string>
            {
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            };

            listView.ItemsSource = observbleCollection;
            listView.ItemSelected += SelctPdfItem;
        }
        private void SelctPdfItem(object sender, SelectedItemChangedEventArgs e)
        {
           // Navigation.PushAsync(new PdfDetailsPage((ListViewModel)e.SelectedItem), true);

            listView.SelectedItem = null;
        }
        private void GotoProfile(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }
    }
}