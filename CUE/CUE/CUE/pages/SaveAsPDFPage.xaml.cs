﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SaveAsPDFPage : ContentPage
	{
		public SaveAsPDFPage ()
		{
			InitializeComponent ();
		}
        void GotoBackStack(object o,EventArgs e) {
            Navigation.PopAsync(true);
        }

    }
}