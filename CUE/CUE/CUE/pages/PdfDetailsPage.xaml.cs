﻿using CUE.BussinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PdfDetailsPage : ContentPage
    {
        ListViewModel selectedItem;
        bool isOn = false;
        public PdfDetailsPage(ListViewModel selectedItem)
        {
            InitializeComponent();
            this.selectedItem = selectedItem;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (selectedItem!=null)
            {
                BindingContext = selectedItem;
            }
        }

        private void Switchfun(object sender, EventArgs e)
        {

            if (isOn)
            {
                switcher.Source = "switchon";
                isOn = false;
            }
            else
            {
                switcher.Source = "switchof";
                isOn = true;
            }
        }
    }
}