﻿using CUE.BussinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CUE.BussinessLogic;
using Plugin.Connectivity;
using System.Globalization;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListOfPdfsHome : ContentPage
    {
        public ListOfPdfsHome()
        {
            InitializeComponent();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            indicater.IsVisible = true;
            CUEService allpdf = new CUEService();
            // var user_id = (Application.Current.Properties["user_id"].ToString());
            //if (!string.IsNullOrEmpty(user_id))
            //{
            if (CrossConnectivity.Current.IsConnected)
            {

                List<ListViewModel> listData = new List<ListViewModel>();
                List<AllPdfDataModel> userdata = await allpdf.AllPdfData();
                for (int i = 0; i < userdata.Count; i++)
                {
                    string finalDate = "", finalexp_data = "";
                    //var date = DateTime.ParseExact("10/14/2016 11:46 AM","MM/dd/yyyy HH:mm tt",CultureInfo.InvariantCulture);
                    if (!string.IsNullOrEmpty(userdata[i].From_date))
                    {
                        //string strData = "17/05/2108";
                        //DateTime dateTime = DateTime.ParseExact(strData, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        //string result = $"{ dateTime.Day} {dateTime.ToString("MMM")}, {dateTime.ToString("yyyy")}";//dateTime.ToString("MMM"
                        DateTime dateTime = DateTime.ParseExact(userdata[i].From_date, "dd/M/yyyy", CultureInfo.InvariantCulture);
                        finalDate = dateTime.Day + System.Environment.NewLine + dateTime.ToString("MMM");
                        DateTime exp_date = DateTime.ParseExact(userdata[i].Exp_date, "dd/M/yyyy", CultureInfo.InvariantCulture);
                        finalexp_data = exp_date.Day + " " + exp_date.ToString("MMM");
                    }
                    ListViewModel listViewModel = new ListViewModel()
                    {
                        Cat_id = "",
                        Data = finalDate,
                        Exp_date = finalexp_data,
                        Flag = "",
                        Header = "",
                        Id = "",
                        Name = "",
                        Prize = "",
                        Remainder = "",
                        Status = "0",
                        Time = "",
                        Url = "",
                        User_id = ""
                    };
                    listData.Add(listViewModel);

                }
                if (listData != null)
                {
                    pdfList.ItemsSource = listData;
                    indicater.IsVisible = false;
                    //listView.ItemSelected += SelctPdfItem;
                }


            }
            else
            {

                indicater.IsVisible = false;
            }
        }

        private async Task SelctPdfItem(object sender, SelectedItemChangedEventArgs e)
        {
            //PdfDetailsPage pdfDetailsPage = new PdfDetailsPage((ListViewModel)e.SelectedItem);
            //await Navigation.PushAsync(pdfDetailsPage);


            pdfList.SelectedItem = null;
        }
        private void GotoProfile(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProfilePage());
        }
    }
}