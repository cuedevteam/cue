﻿using CUE.BussinessLogic;
using CUE.BussinessLogic.Models;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        string user_id = "2";
        public ProfilePage()
        {
            InitializeComponent();
            //user_id = (Application.Current.Properties["user_id"].ToString());
            string user_id = "2";
            indicator.IsVisible = true;
            if (!string.IsNullOrEmpty(user_id))
            {
                GetDataFromUserdata(user_id);
            }
        }

        private async Task GetDataFromUserdata(string user_id)
        {
            if (CrossConnectivity.Current.IsConnected)
            {

                CUEService uEService = new CUEService();
                List<UserUsing_user_idModel> profiledata = await uEService.Userdata(user_id);

                if (profiledata != null)
                {
                    name.Text = profiledata[0].Firstname + " " + profiledata[0].Lastname; ;
                    DOB.Text = profiledata[0].Dbo;
                    mobile_no.Text = profiledata[0].Mobile;
                    email_id.Text = profiledata[0].Email;
                    Password.Text = profiledata[0].Password;
                    Location.Text = profiledata[0].Location;
                    Zip_code.Text = profiledata[0].Zip_code;

                }
                indicator.IsVisible = false;

            }
        }

        void GotoEditProfile(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EditProfilePage());
        }
    }
}