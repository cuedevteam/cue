﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProfilePage : ContentPage
    {
        DatePicker datePicker;

        public EditProfilePage()
        {
            InitializeComponent();
            datePicker = new DatePicker
            {
                Format = "D",
                VerticalOptions = LayoutOptions.CenterAndExpand,

            };

        }
        void OpenDatePicker(object o, EventArgs e)
        {
            datepicker.IsVisible = true;
            datepicker.Focus();
            var date = datepicker.Date;
            datepicker.DateSelected += Changed;
            datepicker.Unfocused += UnFocused;
        }

        private void UnFocused(object sender, FocusEventArgs e)
        {
            datepicker.IsVisible = false;
        }

        private void Changed(object sender, DateChangedEventArgs e)
        {
            var date = e.NewDate;
            day.Text = date.Day.ToString();
            month.Text = date.Month.ToString();
            year.Text = date.Year.ToString();
            datepicker.IsVisible = false;
        }
    }
}