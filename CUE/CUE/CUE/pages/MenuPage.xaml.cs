﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : MasterDetailPage
    {
        public MenuPage()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MenuPageMenuItem;
            if (item == null)
                return;

            //var page = (Page)Activator.CreateInstance(item.TargetType);
            //page.Title = item.Title;

            //Detail = new NavigationPage(page);
           
            switch (item.ItemName)
            {
                case "Profile":
                    IsPresented = false;
                    Detail = new NavigationPage(new ProfilePage());
                   
                    break;
                case "Dashboard":
                    IsPresented = false;
                    Detail = new NavigationPage(new DashBoardPage());
                  
                    break;
                case "Settings":
                    IsPresented = false;
                    Detail = new NavigationPage(new SettingsPage());
            
                    break;
                case "Logout":
                    IsPresented = false;
                    Navigation.PushModalAsync(new LoginPage());
                    break;

            }
            MasterPage.ListView.SelectedItem = null;
        }
    }
}