﻿using CUE.BussinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OTPPage : ContentPage
    {
        
        string Email, OTP;
        public OTPPage()
        {
            InitializeComponent();
            

        }
        public void CheckOTP(object obj, EventArgs e)
        {

            if (string.IsNullOrEmpty(otp1.Text) && string.IsNullOrEmpty(otp2.Text) && string.IsNullOrEmpty(otp3.Text) && string.IsNullOrEmpty(otp4.Text))
            {
                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter OTP sented to your register mail Id", 5000, null, null);
            }
            else
            {
                OTP = (Application.Current.Properties["OTP"].ToString());
                string otp = otp1.Text + otp2.Text + otp3.Text + otp4.Text + otp5.Text;
                if (otp.Equals(OTP))
                {
                    Navigation.PushModalAsync(new HomePage());
                }
                else
                {

                    //var options = new NotificationOptions()
                    //{
                    //    Title = registerModel.Result.Status_message
                    //};
                    //var result =  notificator.Notify(options);
                }

            }
        }
    }
}