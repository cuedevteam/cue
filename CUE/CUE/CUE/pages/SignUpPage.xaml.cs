﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CUE.BussinessLogic;
using CUE.pages;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        // https://github.com/EgorBo/Toasts.Forms.Plugin
        public const string emailRegex = @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
        public SignUpPage()
        {
            InitializeComponent();
            indicater.IsVisible = false;
        }
        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async Task GoToOTPPage(object sender, EventArgs e)
        {
            //check entrys are empty if empty show mesg else check validations for username,phoNo 
            //all ok call API 
            string Email, Passwrd, PhNo;
            Email = username.Text;
            Passwrd = password.Text;
            PhNo = phoNo.Text;
            if (string.IsNullOrEmpty(Email))
            {
                // DisplayAlert("", " Enter Email Id", "ok","Cancel");

                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Email Id", 5000, null, null);

            }
            else if (string.IsNullOrEmpty(Passwrd))
            {
                // DisplayAlert("", " Enter Passwrd ", "ok", "Cancel");
                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Passwrd", 5000, null, null);
            }
            else if (string.IsNullOrEmpty(PhNo))
            {
                //DisplayAlert("", " Enter Phone Number ", "ok", "Cancel");
                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Phone Number", 5000, null, null);

            }
            else if (!Regex.IsMatch(Email, emailRegex))
            {
                //DisplayAlert("", " Enter Valid Email Id ", "ok", "Cancel");
                DependencyService.Get<CUE.interfaces.INotification>().Notify("Enter Valid Email Id", 5000, null, null);

            }
            else
            {

                //call service for registerations 
                if (CrossConnectivity.Current.IsConnected)
                {
                    indicater.IsVisible = true;
                    CUEService cueservice = new CUEService();
                    RegisterModel registerModel = await cueservice.RegisterAsync(Email, Passwrd, PhNo);
                    if (registerModel.Status)
                    {

                        DependencyService.Get<CUE.interfaces.INotification>().Notify(registerModel.Status_message, 5000, null, null);

                        //For Saving Value
                        Application.Current.Properties["Email"] = Email;
                        await Application.Current.SavePropertiesAsync();

                        //send OTP to register email id 
                        CUEService cueservice1 = new CUEService();

                        var registerModel1 = await cueservice1.SendMail(Email);
                        if (registerModel1.Status)
                        {
                            DependencyService.Get<CUE.interfaces.INotification>().Notify(registerModel1.Status_message, 5000, null, null);
                            Application.Current.Properties["OTP"] = registerModel1.Otp;
                            await Application.Current.SavePropertiesAsync();

                        }
                        indicater.IsVisible = false;
                        await Navigation.PushModalAsync(new OTPPage());

                    }
                    else
                    {
                        indicater.IsVisible = false;
                    }
                }
                else
                {
                    indicater.IsVisible = false;
                }
            }
        }
    }
}