﻿using DLToolkit.Forms.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CUE.pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DashBoardPage : ContentPage
	{
        ObservableCollection<TaskList> Data;
        public DashBoardPage ()
		{
			InitializeComponent ();
            FlowListView.Init();
            Data = new ObservableCollection<TaskList> { new TaskList { Count = "2", TaskStatus = "task Started" }, new TaskList { Count = "4", TaskStatus = "task in progress" }, new TaskList { Count = "20", TaskStatus = "task completed" } };
            ListsOfCategories.FlowItemsSource = Data;
        }
        public void FlowListView_OnFlowItemTapped(object sender, ItemTappedEventArgs e)
        {
            var seletedItem = e.Item as TaskList;
          //  DisplayAlert("OnFlowItemDisappearing", seletedItem.TaskStatus, "ok");
        }
        private class TaskList
        {
            public string Count { get; set; }
            public string TaskStatus{ get; set; }
        }
    }
}