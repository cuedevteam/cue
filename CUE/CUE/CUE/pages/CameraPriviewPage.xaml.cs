﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.Diagnostics;
using Microsoft.ProjectOxford.Vision.Contract;
using Microsoft.ProjectOxford.Vision;
using System.Collections.ObjectModel;
using Android.Speech.Tts;
using static CUE.pages.CameraPriviewPage;

namespace CUE.pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CameraPriviewPage : ContentPage
    {
        OcrResults text;
        ObservableCollection<InVoice> Invoice;
        public CameraPriviewPage()
        {
            InitializeComponent();
           
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await CrossMedia.Current.Initialize();
        }
        void GotoSaveFile(object o, EventArgs e)
        {

            Navigation.PushAsync(new SaveAsPDFPage());
        }
        async Task OpenGalleryAsync(object o, EventArgs e)
        {
            try
            {
                var file = await CrossMedia.Current.PickPhotoAsync();
                //open gallery 
                //if (IsPickPhotoSupported) { }
               
                if (file == null)
                    return;

                // image set to your image view====
                
                imagetopdf.Source = ImageSource.FromStream(() => file.GetStream());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error  == ", ex.Message);
            }


        }
        async Task OpenCameraAsync(object o, EventArgs e)
        {
          

            MediaFile photo;
            if (CrossMedia.Current.IsCameraAvailable)
            {
                photo = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    Directory = "Invoices",
                    Name = "Invoice.jpg"
                });
            }
            else
            {
                photo = await CrossMedia.Current.PickPhotoAsync();
            }
            var client = new VisionServiceClient("d02a5cbfa7dd40e79f21d25363eef251");
            using (var photoStream = photo.GetStream())
            {
                text = await client.RecognizeTextAsync(photoStream);
            }
           // samplp.Text = text.Regions[0].Lines[0].Words[0].Text;


            double total = 0.0;
            foreach (var region in text.Regions)
            {
                foreach (var line in region.Lines)
                {
                    foreach (var word in line.Words)
                    {
                        if (word.Text.Contains("$"))
                        {
                            var number = Double.Parse(word.Text.Replace("$", ""));
                            total = (number > total) ? number : total;
                        }
                    }
                }
            }
            Invoice = new ObservableCollection<InVoice>
            {
                // Add to data-bound collection.
                new InVoice
                {
                    Total = total,
                    Photo = photo.Path,
                    TimeStamp = DateTime.Now
                }
            };

            ////open Camera 
            //var file  = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            //{
            //    SaveToAlbum = true,
            //    Name = "IMG.jpg"
            //});
            //imagetopdf.Source = ImageSource.FromStream(() => file.GetStream());
        }

        public class InVoice
        {
            public double Total { get; set; }
            public string Photo { get; set; }
            public DateTime TimeStamp { get; set; }

        }
    }
}