﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CUE.interfaces
{
   public interface INotification
    {
        void Notify(string message, int duration, string action, Action<object> callback);
    }
}
